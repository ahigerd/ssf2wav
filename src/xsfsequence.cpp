#include "xsfsequence.h"
#include "utility.h"
#include "ssfnode.h"
#include "XSFCommon.h"
#include <stdexcept>

XSFSequence::XSFSequence(SynthContext* ctx, const OpenFn& openFile)
: BaseSequence(ctx->s2wContext()), openFile(openFile), ctx(ctx), gain(1.0)
{
  // initializers only
}

void XSFSequence::setGain(double value)
{
  gain = value;
}

void XSFSequence::loadXSF(const std::string& path, std::istream& xsfStream)
{
  std::vector<const XSFFile*> xsfs;
  xsf.ReadXSF(xsfStream);

  std::string libFile = xsf.GetTagValue("_lib");
  if (!libFile.empty()) {
    auto libStream(openFile(ExtractDirectoryFromPath(path) + libFile));
    lib.ReadXSF(*libStream.get());
    xsfs.push_back(&lib);
  }
  xsfs.push_back(&xsf);

  std::shared_ptr<AudioNode> node;
  if (xsf.IsValidType(0x11)) {
    node.reset(new SsfNode(xsfs, ctx, 1));
  } else if (xsf.IsValidType(0x12)) {
    node.reset(new SsfNode(xsfs, ctx, 2));
  } else {
    throw std::runtime_error("Not a SSF or DSF file");
  }

  BasicTrack* track = new BasicTrack;
  track->addEvent(new ChannelEvent(AudioNode::Gain, gain));
  AudioNodeEvent* event = new AudioNodeEvent(node);
  event->timestamp = 0;
  event->duration = xsf.GetLengthMS(120000) / 1000.0;
  double fade = xsf.GetFadeMS(0) / 1000.0;
  if (fade > 0) {
    event->setEnvelope(0, 0, 1.0, fade);
  }
  track->addEvent(event);
  if (fade > 0) {
    track->addEvent(new KillEvent(event->playbackID, event->duration));
    KillEvent* kill = new KillEvent(event->playbackID, event->duration + fade);
    kill->immediate = true;
    track->addEvent(kill);
  }
  addTrack(track);
  ctx->addChannel(track);
}

double XSFSequence::duration() const
{
  return getXsfLength(&xsf);
}
