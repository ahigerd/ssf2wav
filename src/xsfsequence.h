#ifndef SSF2WAV_XSFSEQUENCE_H
#define SSF2WAV_XSFSEQUENCE_H

#include "seq/isequence.h"
#include "seq/itrack.h"
#include "synth/synthcontext.h"
#include "plugin/baseplugin.h"
#include "XSFFile.h"
#include <fstream>

class XSFSequence : public BaseSequence<BasicTrack> {
public:
  XSFSequence(SynthContext* ctx, const OpenFn& openFile = openFstream);

  void setGain(double value);
  void loadXSF(const std::string& path, std::istream& xsfStream);

  double duration() const;
  inline const XSFFile* xsfFile() const { return &xsf; }

private:
  OpenFn openFile;
  XSFFile xsf, lib;
  SynthContext* ctx;
  double gain;
};

#endif
