#include "xsfsequence.h"
#include "riffwriter.h"
#include "synth/synthcontext.h"
#include "synth/channel.h"
#include "commandargs.h"
#include "Core/sega.h"
#include "XSFCommon.h"
#include <fstream>
#include <iostream>
#include <iomanip>

int main(int argc, char** argv)
{
  CommandArgs args({
    { "help", "h", "", "Show this help text" },
    { "output", "o", "filename", "Set the output filename (default: input filename with .wav extension)" },
    { "verbose", "v", "", "Outputs additional information about the input file" },
    { "gain", "g", "amount", "Adjusts the gain (volume) of the audio (default: 1.0)" },
    { "", "", "input", "Path to a .ssf, .dsf, .minissf, or .minidsf file" },
  });
  std::string argError = args.parse(argc, argv);
  if (!argError.empty()) {
    std::cerr << argError << std::endl;
    return 1;
  } else if (args.hasKey("help") || argc < 2) {
    std::cout << args.usageText(argv[0]) << std::endl;
    return 0;
  } else if (args.positional().size() != 1) {
    std::cerr << argv[0] << ": exactly one input filename required" << std::endl;
    return 1;
  }

  std::string infile = args.positional().at(0);
  std::unique_ptr<std::istream> xsfStream = openFstream(infile);
  if (!xsfStream) {
    std::cerr << argv[0] << ": error opening file" << std::endl;
    return 1;
  }

  if (sega_init()) {
    std::cerr << argv[0] << ": error while initializing core" << std::endl;
    return 1;
  }

  S2WContext s2w;
  SynthContext ctx(&s2w, 44100, 2);
  XSFSequence seq(&ctx);
  seq.setGain(args.getFloat("gain", 1.0));
  seq.loadXSF(infile, *xsfStream);
  if (args.hasKey("verbose")) {
    auto xsfTags = seq.xsfFile()->GetAllTags();
    std::cerr << "Tags in " << infile << ":" << std::endl;
    std::string libFile;
    for (const std::string& key : xsfTags.GetKeys()) {
      std::cerr << "  " << key << " = " << xsfTags[key] << std::endl;
      if (key == "_lib") {
        libFile = xsfTags[key];
      }
    }
    std::cerr << std::endl;
    if (!libFile.empty()) {
      std::cerr << "Tags in " << libFile << ":" << std::endl;
      XSFFile lib(ExtractDirectoryFromPath(infile) + libFile);
      xsfTags = lib.GetAllTags();
      for (const std::string& key : xsfTags.GetKeys()) {
        std::cerr << "  " << key << " = " << xsfTags[key] << std::endl;
      }
      std::cerr << std::endl;
    }
  }
  std::string filename = args.getString("output", infile + ".wav");
  std::cerr << "Writing " << (int(ctx.maximumTime() * 10) * .1) << " seconds to \"" << filename << "\"..." << std::endl;
  RiffWriter riff(ctx.sampleRate, true);
  riff.open(filename);
  ctx.save(&riff);
  riff.close();
  return 0;
}

