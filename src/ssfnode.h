#ifndef SSF2WAV_SSFNODE_H
#define SSF2WAV_SSFNODE_H

#include "buffernode.h"
class XSFFile;

class SsfNode : public BufferNode {
public:
  SsfNode(const std::vector<const XSFFile*>& xsfs, const SynthContext* ctx, int version);
  ~SsfNode();

protected:
  virtual int fillBuffer(std::vector<int16_t>& buf);

  std::vector<uint8_t> state;
  void* sega;
  void* yam;
};

double getXsfLength(const XSFFile* xsf);

#endif
