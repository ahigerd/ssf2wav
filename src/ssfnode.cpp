#include "ssfnode.h"
#include "utility.h"
#include "XSFFile.h"
#include "Core/yam.h"
#include "Core/sega.h"
#include "Core/satsound.h"
#include "Core/dcsound.h"

double getXsfLength(const XSFFile* xsf)
{
  return (xsf->GetLengthMS(120000) + xsf->GetFadeMS(0)) / 1000.0;
}

SsfNode::SsfNode(const std::vector<const XSFFile*>& xsfs, const SynthContext* ctx, int version)
: BufferNode(getXsfLength(xsfs.back()), ctx), state(sega_get_state_size(version)), sega(state.data())
{
  sega_clear_state(sega, version);
  sega_enable_dry(sega, 0);
  sega_enable_dsp(sega, 1);
  sega_enable_dsp_dynarec(sega, 1);

  if (version == 1) {
    yam = satsound_get_yam_state(sega_get_satsound_state(sega));
  } else {
    yam = dcsound_get_yam_state(sega_get_dcsound_state(sega));
  }
  if (yam) {
    yam_prepare_dynacode(yam);
  }

  for (auto xsf : xsfs) {
    auto program = xsf->GetProgramSection();
    sega_upload_program(sega, program.data(), program.size());
  }
}

SsfNode::~SsfNode()
{
  if (yam) {
    yam_unprepare_dynacode(yam);
  }
}

int SsfNode::fillBuffer(std::vector<int16_t>& buf)
{
  uint32_t samples = buf.size() >> 1;
  int ok = sega_execute(state.data(), 0x7FFFFFFF, buf.data(), &samples);
  if (ok < 0) {
    return -1;
  }
  return samples << 1;
}
