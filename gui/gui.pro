isEmpty(BUILDPATH) {
  error("BUILDPATH must be set")
}
BUILDPATH = $$absolute_path($$BUILDPATH)
include($$BUILDPATH/../seq2wav/gui/gui.pri)
CONFIG += c++17

INCLUDEPATH += $$PROJPATH/highly_theoretical

SOURCES += main.cpp ../plugins/s2wplugin.cpp
