ssf2wav
=======

ssf2wav is a player for Saturn and Dreamcast video game music rips in ssf and dsf formats
based on the [Highly Theoretical](https://gitlab.com/kode54/highly_theoretical) library
by kode54.

Prebuilt plugins are available in the [downloads section](https://bitbucket.org/ahigerd/ssf2wav/downloads/):

* (Windows) Winamp: [in_ssf2wav.dll](https://bitbucket.org/ahigerd/ssf2wav/downloads/in_ssf2wav.dll)
* (Ubuntu) Audacious: [aud_ssf2wav.so](https://bitbucket.org/ahigerd/ssf2wav/downloads/aud_ssf2wav.so)

While this project can generate a plugin for Foobar2000, the original plugin that ssf2wav
is based on, [SSF/DSF Decoder](https://www.foobar2000.org/components/view/foo_input_ht) by
kode54, is recommended.

Building
--------
To build on POSIX platforms or MinGW using GNU Make, simply run `make`. The following make
targets are recognized:

* `cli`: builds the command-line tool. (default)
* `plugins`: builds all plugins supported by the current platform.
* `all`: builds the command-line tool and all plugins supported by the current platform.
* `debug`: builds a debug version of the command-line tool.
* `audacious`: builds just the Audacious plugin, if supported.
* `winamp`: builds just the Winamp plugin, if supported.
* `foobar`: builds just the Foobar2000 plugin, if supported.
* `aud_(NAME)_d.dll`: builds a debug version of the Audacious plugin, if supported.
* `in_(NAME)_d.dll`: builds a debug version of the Winamp plugin, if supported.

The following make variables are also recognized:

* `CROSS=mingw`: If building on Linux, use MinGW to build Windows binaries.
* `CROSS=msvc`: Use Microsoft Visual C++ to build Windows binaries, using Wine if the current
  platform is not Windows. (Required to build the Foobar2000 plugin.)
* `WINE=[command]`: Sets the command used to run Wine. (Default: `wine`)

To build using Microsoft Visual C++ on Windows without using GNU Make, run `buildvs.cmd`,
optionally with one or more build targets. The following build targets are supported:

* `cli`: builds the command-line tool. (default)
* `plugins`: builds the Winamp and Foobar2000 plugins.
* `all`: builds the command-line tool and the Winamp and Foobar2000 plugins.
* `winamp`: builds just the Winamp plugin.
* `foobar`: builds just the Foobar2000 plugin.

Separate debug builds are not supported with Microsoft Visual C++, but the build flags may be
edited in `msvc.mak`.

License
-------
ssf2wav is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the [GNU General Public License](LICENSE-GPL3.txt)
along with this program.  If not, see https://www.gnu.org/licenses/.


Portions of this project are based on seq2wav, in_xsf_framework,
and zlib. Licensing information about these sources are available
in the [LICENSE.md](LICENSE.md) file.
