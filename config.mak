PLUGIN_NAME = ssf2wav
include $(ROOTPATH)seq2wav/config.mak
CXXFLAGS := $(CXXFLAGS) -I../highly_theoretical
CXXFLAGS_R := $(CXXFLAGS_R) -I../highly_theoretical
CXXFLAGS_D := $(CXXFLAGS_D) -I../highly_theoretical
LDFLAGS_R := $(LDFLAGS_R) ../$(BUILDPATH)/SegaCore.a
LDFLAGS_D := $(LDFLAGS_D) ../$(BUILDPATH)/SegaCore_d.a

ifeq ($(OS),Windows_NT)
	CXXFLAGS_R := $(CXXFLAGS_R) -Izlib
	CXXFLAGS_D := $(CXXFLAGS_D) -Izlib
	CFLAGS_R := $(patsubst -std=%,,$(CXXFLAGS_R))
	CFLAGS_D := $(patsubst -std=%,,$(CXXFLAGS_D))
else
	LDFLAGS_R := $(LDFLAGS_R) -lz
	LDFLAGS_D := $(LDFLAGS_D) -lz
endif
