#include "XSFFile.h"
#include "plugin/baseplugin.h"
#include "codec/sampledata.h"
#include "synth/synthcontext.h"
#include "xsfsequence.h"
#include "Core/sega.h"
#include <sstream>

// In the functions below, openFile() is provided by the plugin interface. Use this
// instead of standard library functions to open additional files in order to use
// the host's virtual filesystem.

static bool didInit = false;

std::unique_ptr<std::istream> makeBufferStream(std::istream& source)
{
  std::stringstream* stream = new std::stringstream();
  std::unique_ptr<std::istream> uniqStream(stream);
  char buffer[1024];
  while (source) {
    source.read(buffer, 1024);
    int bytes = source.gcount();
    if (bytes > 0) {
      stream->write(buffer, bytes);
    }
  }
  return uniqStream;
}

struct S2WPluginInfo {
  S2WPLUGIN_STATIC_FIELDS

  static bool isPlayable(std::istream& file) {
    // Implementations should check to see if the file is supported.
    // Return false or throw an exception to report failure.
    char header[4];
    if (!file.read(header, 4)) {
      return false;
    }
    if (header[0] != 'P' || header[1] != 'S' || header[2] != 'F') {
      return false;
    }
    return header[3] == 0x11 || header[3] == 0x12;
  }

  static int sampleRate(S2WContext*, const std::string&, std::istream&) {
    // Implementations should return the sample rate of the file.
    // This can be hard-coded if the plugin always uses the same sample rate.
    return 44100;
  }

  static double length(S2WContext*, const std::string&, std::istream& file) {
    // Implementations should return the length of the file in seconds.
    auto buffer(makeBufferStream(file));
    XSFFile xsf;
    xsf.ReadXSF(*buffer.get(), 0, 0, true);
    return (xsf.GetLengthMS(120000) + xsf.GetFadeMS(5000)) / 1000.0;
  }

  static TagMap readTags(S2WContext*, const std::string&, std::istream& file) {
    // Implementations should read the tags from the file.
    // If the file format does not support embedded tags, consider
    // inheriting from TagsM3UMixin and removing this function.
    auto buffer(makeBufferStream(file));
    XSFFile xsf;
    xsf.ReadXSF(*buffer.get(), 0, 0, true);
    const auto xsfTags = xsf.GetAllTags();
    TagMap tags;
    for (const std::string& key : xsfTags.GetKeys()) {
      tags[key] = xsfTags[key];
    }
    return tags;
  }

  SynthContext* prepare(S2WContext* s2w, const std::string& filename, std::istream& file) {
    // Prepare to play the file. Load any necessary data into memory and store any
    // applicable state in members on this plugin object.
    if (!didInit) {
      if (sega_init()) {
        throw std::runtime_error("error while initializing core");
      }
      didInit = true;
    }
    auto buffer(makeBufferStream(file));
    ctx.reset(new SynthContext(s2w, 44100, 2));
    seq.reset(new XSFSequence(ctx.get(), OpenFn([s2w](const std::string& path) {
      auto source(s2w->openFile(path));
      return makeBufferStream(*source.get());
    })));
    seq->loadXSF(filename, *buffer.get());
    return ctx.get();
  }

  void release() {
    // Release any retained state allocated in prepare().
    seq.reset(nullptr);
    ctx.reset(nullptr);
  }

  std::unique_ptr<SynthContext> ctx;
  std::unique_ptr<XSFSequence> seq;
};

const std::string S2WPluginInfo::version = "0.1.2";
const std::string S2WPluginInfo::pluginName = "ssf2wav Saturn/Dreamcast Plugin";
const std::string S2WPluginInfo::pluginShortName = "ssf2wav";
ConstPairList S2WPluginInfo::extensions = {
  { "ssf", "Saturn Sound Format (*.ssf)" },
  { "minissf", "Saturn Sound Format with library (*.minissf)" },
  { "dsf", "Dreamcast Sound Format (*.dsf)" },
  { "minidsf", "Dreamcast Sound Format with library (*.minidsf)" },
};
const std::string S2WPluginInfo::about =
  "ssf2wav Plugin copyright (C) 2021 Adam Higerd\n"
  "Distributed under the GPLv3 license.";

SEQ2WAV_PLUGIN(S2WPluginInfo);
