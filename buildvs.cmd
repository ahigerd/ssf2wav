@echo off
echo. 2> depends.tmp
for /r %%f in (*.cpp) do (
  SET "_o=%%f"
  call cmd /c if "%%_o:main.cpp=%%"=="%%_o%%" echo "ssf2wav.exe" "in_ssf2wav.dll" "aud_ssf2wav.dll": "%%_o:~0,-4%%.obj">>depends.tmp
  call cmd /c if "%%_o:main.cpp=%%"=="%%_o%%" echo "foo_input_ssf2wav.dll": "%%_o:~0,-4%%.obj">>depends.tmp
  echo.>>depends.tmp
)
for /r %%f in (*.c) do (
  SET "_o=%%f"
  call cmd /c echo "ssf2wav.exe" "in_ssf2wav.dll" "aud_ssf2wav.dll": "%%_o:~0,-2%%.obj">>depends.tmp
  call cmd /c echo "foo_input_ssf2wav.dll": "%%_o:~0,-2%%.obj">>depends.tmp
  echo.>>depends.tmp
)
type depends.tmp | find \src\ > depends.mak
del depends.tmp
if [%*] NEQ [depends] nmake /f msvc32.mak %*
